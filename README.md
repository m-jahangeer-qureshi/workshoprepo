## The central repository for all the workshops

This repository will contain the basic code and resources required to setup the environment for the workshops

## Setup

* This setup was tested on Ubuntu 18.04 and is expected to function similarly across its versions\

* Create a conda environment or a virtual environment for a seperate python environment
consult either the anaconda docs (https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) or the venv docs (https://docs.python.org/3/library/venv.html) to do so.

* Once setup, install the neccesary libraries as follows
 ```
 pip install -r requirements.txt
 ```
* and download the ML model used in the workshop as follows
 ```
 python MLModel.py
 ```
* You should now be ready for the workshop