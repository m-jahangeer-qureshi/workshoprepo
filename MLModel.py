import pandas as pd
from transformers import pipeline

class MLModel():
    def __init__(self, model='bhadresh-savani/distilbert-base-uncased-emotion'):
        '''
        Initializes the classifier pipeline with an emotion classifier
        '''
        self.classifier = pipeline("text-classification",model=model, return_all_scores=True)
        
    def __call__(self, text):
        '''
        Given a string the model returns prediction the emotions of the given string
        '''
        prediction = self.classifier(text, )[0]
        return prediction

if __name__ == "__main__":
    ml_model = MLModel()
    ml_model("hello world")